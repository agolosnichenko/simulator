class AddBotToSteps < ActiveRecord::Migration[5.2]
  def change
    add_reference :steps, :bot, index: true
  end
end
