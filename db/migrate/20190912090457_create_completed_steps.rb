class CreateCompletedSteps < ActiveRecord::Migration[5.2]
  def change
    create_table :completed_steps do |t|
      t.references :step, index: true
      t.references :user, index: true
      t.string :useranswer
      t.boolean :correct
      t.timestamps
    end
    add_foreign_key :completed_steps, :steps
    add_foreign_key :completed_steps, :users
    add_index :completed_steps, [:step_id, :user_id], :unique => true
  end
end
