class RemoveAvatarFromSteps < ActiveRecord::Migration[5.2]
  def change
    remove_column :steps, :from, :string
    remove_column :steps, :avatar, :string
  end
end
