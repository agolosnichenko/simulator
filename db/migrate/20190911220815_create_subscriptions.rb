class CreateSubscriptions < ActiveRecord::Migration[5.2]
  def change
    create_table :subscriptions do |t|
      t.references :course, index: true
      t.references :user, index: true
      t.timestamps
    end
    add_foreign_key :subscriptions, :courses
    add_foreign_key :subscriptions, :users
    add_index :subscriptions, [:course_id, :user_id], :unique => true
  end
end
