class CreateCompletedLessons < ActiveRecord::Migration[5.2]
  def change
    create_table :completed_lessons do |t|
      t.references :lesson, index: true
      t.references :user, index: true
      t.integer :correctanswers
      t.timestamps
    end
    add_foreign_key :completed_lessons, :lessons
    add_foreign_key :completed_lessons, :users
    add_index :completed_lessons, [:lesson_id, :user_id], :unique => true
  end
end
