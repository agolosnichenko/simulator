class AddTagToSteps < ActiveRecord::Migration[5.2]
  def change
    add_column :steps, :tag, :integer
  end
end
