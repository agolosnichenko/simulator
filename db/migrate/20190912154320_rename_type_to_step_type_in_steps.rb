class RenameTypeToStepTypeInSteps < ActiveRecord::Migration[5.2]
  def change
    rename_column :steps, :type, :step_type
  end
end
