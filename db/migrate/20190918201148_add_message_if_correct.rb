class AddMessageIfCorrect < ActiveRecord::Migration[5.2]
  def change
    add_column :steps, :message_if_correct, :string
    add_column :steps, :message_if_incorrect, :string
  end
end
