Rails.application.routes.draw do
  root 'pages#home'
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)
  devise_for :users, components: {registrations: 'users/registrations', sessions: 'users/sessions'}

  get '/step/get_completed_steps/:lesson_id', to: 'step#get_completed_steps'
  get '/step/complete_step/', to: 'step#complete_step'
  get '/step/get_next_step/', to: 'step#get_next_step'

  resources :course do
    resources :lesson, only: [:show]
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
