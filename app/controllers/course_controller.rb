class CourseController < ApplicationController
  before_action :authenticate_user!
  def show
    @user = current_user
    @course = Course.find(params[:id])
    @lessons = @course.lessons.order(:tag)
    user_completed_lessons = current_user.completed_lessons.where({lesson_id: @lessons})
    if user_completed_lessons.count > 0
      last_completed_lesson_tag = @lessons.where({id: user_completed_lessons.pluck(:lesson_id)}).order(:tag).last.tag
      @active_lesson = Lesson.find_by tag: last_completed_lesson_tag + 1
    else
      @active_lesson = @lessons.first
    end

    if current_user.subscriptions.where(course_id: @course.id).count == 0
      Subscription.create!(course_id: @course.id, user_id: @user.id)
    end

    redirect_to proc {course_lesson_url(@course,@active_lesson)}
  end

  def index
    @courses = Course.all
  end

end
