class LessonController < ApplicationController
  before_action :authenticate_user!
  def show
    @user = current_user
    @course = Course.find(params[:course_id])
    @lessons = @course.lessons.order(:tag)
    @active_lesson = Lesson.find(params[:id])

    if @user.completed_lessons.where(lesson_id: @lessons).count > 0 then
      all_completed_lessons_of_user = @user.completed_lessons.where(lesson_id: @lessons.pluck(:id))
      last_completed_lesson_of_user = Lesson.where(id: all_completed_lessons_of_user.pluck(:lesson_id)).order(:tag).last

      if Lesson.find_by_tag(last_completed_lesson_of_user.tag + 1) then
        @last_lesson = Lesson.find_by_tag(last_completed_lesson_of_user.tag + 1)
      else
        @last_lesson = @active_lesson;
      end
    end

    @steps = @active_lesson.steps
  end
end
