class StepController < ApplicationController

  #Функция получения завершенных шагов
  def get_completed_steps
    @lesson = Lesson.find(params[:lesson_id])
    @total_lessons = Course.find(@lesson.course_id).lessons.count
    @steps = @lesson.steps.where({id: current_user.completed_steps.pluck(:step_id)}).order(:tag)
    if !@steps.exists? then
      @steps = @lesson.steps.where({tag: 0})
      @step = @lesson.steps.first
      current_user.completed_steps.create!(step_id: @step.id, user_id: current_user.id)
    end

    render json: @steps, meta: {total_lessons: @total_lessons, current_lesson: @lesson ,total_steps: @lesson.steps.count, total_tests: @lesson.steps.where(step_type: ['test_input', 'test_option', 'test_checkbox']).count, user_name: current_user.name, user_avatar: current_user.avatar.attached? ? rails_blob_path(current_user.avatar, only_path: true) : nil}, root: "steps"

  end

# Функция завершения шага
  def complete_step
    respond_to do |format|
      format.json {
        @step = Step.find(params[:step_id])
        @lesson = Lesson.find_by_id(@step.lesson_id)
        @user = current_user
        if params.has_key?(:user_answer) then
          @user_answer = params[:user_answer]
          if @user_answer == @step.correctanswer
            @correct = true
          else
            @correct = false
          end
          request = @user.completed_steps.create!(step_id: @step.id, user_id: @user.id, useranswer: @user_answer, correct: @correct)
        else
          request = @user.completed_steps.create!(step_id: @step.id, user_id: @user.id)
        end

        if @user.completed_steps.where({step_id: @lesson.steps}).count == @lesson.steps.count
          @user.completed_lessons.create!(lesson_id: @lesson.id, user_id: @user.id, correctanswers: @user.completed_steps.where({step_id: @lesson.steps, correct: 1}).count)
          if @user.completed_lessons.find_by_lesson_id(@lesson.id).correctanswers != nil
            @correct_answers = @user.completed_lessons.find_by_lesson_id(@lesson.id).correctanswers
          else
            @correct_answers = nil
          end
        end

        if request
          render json: {status: 'ok', correct: @correct, correct_answers: @correct_answers}.to_json
        else
          render json: {status: 'error'}.to_json
        end
      }

     end

  end

# Функция получения следующего шага
  def get_next_step
    respond_to do |format|
      format.json {
        @current_step = Step.find(params[:current_step])
        @next_step = Step.find_by_tag(@current_step.tag + 1)
        render json: @next_step
      }
    end
  end

end
