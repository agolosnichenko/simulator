//= require active_admin/base
//= require tinymce

$(document).ready(function() {
  tinyMCE.init({
     mode: 'textareas',
     theme: 'silver',
     plugins: ['image', 'link', 'code', 'lists'],
     toolbar: ['undo redo | bold italic underline strikethrough | fontselect fontsizeselect formatselect | alignleft aligncenter alignright alignjustify | outdent indent |  numlist bullist checklist | image link']
   });
});
