class CompletedStep < ApplicationRecord
  belongs_to :step
  belongs_to :user
end
