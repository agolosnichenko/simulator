class Step < ApplicationRecord
  serialize :content
  belongs_to :lesson
  has_many :completed_steps
  belongs_to :bot
end
