class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_many :subscriptions
  has_many :courses, through: :subscriptions
  has_many :completed_lessons
  has_many :completed_steps
  has_one_attached :avatar
end
