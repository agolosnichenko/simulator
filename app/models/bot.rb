class Bot < ApplicationRecord
  has_many :steps
  has_one_attached :avatar
end
