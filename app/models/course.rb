class Course < ApplicationRecord
  has_many :users, through: :subscriptions
  has_many :subscriptions
  has_many :lessons
  has_one_attached :image
end
