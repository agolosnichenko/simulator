class CompletedStepSerializer < ActiveModel::Serializer
  attribute :useranswer, if: :condition?
  attribute :correct, if: :condition?

  def condition?
    object.user_id == current_user.id
  end

  belongs_to :step

end
