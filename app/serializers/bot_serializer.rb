class BotSerializer < ActiveModel::Serializer
  include Rails.application.routes.url_helpers
  attributes :id, :name, :position, :avatar_url
  has_many :steps

  def avatar_url
    rails_blob_path(object.avatar, only_path: true) if object.avatar.attachment
  end

end
