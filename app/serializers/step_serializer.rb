class StepSerializer < ActiveModel::Serializer
  attributes :id, :tag, :step_type, :content, :option1, :option2, :option3,
              :option4, :option5, :option6, :option7, :option8,
              :message_if_correct, :message_if_incorrect

  has_many :completed_steps
  belongs_to :bot
end
