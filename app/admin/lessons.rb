ActiveAdmin.register Lesson do
# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  permit_params :name, :tag, :course_id
  sortable tree: false, sorting_attribute: :tag
  index :as => :sortable do
    label :name
    actions
  end
  index do
    selectable_column
    column :name, label: "Название"
    column :tag, label: "Тег"
    column :course, label: "Курс"
    actions
  end

  form do |f|
    f.inputs do
      input :course, label: "Курс"
      input :name, label: "Название"

    end
    actions
  end

end
