ActiveAdmin.register Bot do
  permit_params :name, :position, :avatar

  show do |t|
    attributes_table do
      row :name, label: "Имя"
      row :position, label: "Должность"
      row :avatar, label: "Аватар"
      end
    end

  form :html => {:enctype => "multipart/form-data"} do |f|
    f.inputs do
      f.input :name, label: "Имя"
      f.input :position, label: "Должность"
      f.file_field :avatar, label: "Аватар"
    end
    f.actions
  end

end
