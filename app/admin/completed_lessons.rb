ActiveAdmin.register CompletedLesson do
  permit_params :user_id, :lesson_id

  show do |t|
    attributes_table do
      row :user_id, label: "Пользователь"
      row :lesson_id, label: "Урок"
      row :correctanswers, label: "Правильных ответов"
      end
    end

  form :html => {:enctype => "multipart/form-data"} do |f|
    f.inputs do
      f.input :user_id, label: "Пользователь"
      f.input :lesson_id, label: "Урок"
    end
    f.actions
  end

end
