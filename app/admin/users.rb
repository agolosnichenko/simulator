ActiveAdmin.register User do
  permit_params :name, :surname, :email, :phone, :password, :password_confirmation, :avatar

  index do
    selectable_column
    id_column
    column :name
    column :surname
    column :email
    column :phone
    column :current_sign_in_at
    column :sign_in_count
    column :created_at
    actions
  end

  filter :email
  filter :current_sign_in_at
  filter :sign_in_count
  filter :created_at

  form do |f|
    f.inputs do
      f.input :name
      f.input :surname
      f.input :email
      f.input :phone
      f.input :password
      f.input :password_confirmation
      f.file_field :avatar, label: "Аватар"
    end
    f.actions
  end

end
