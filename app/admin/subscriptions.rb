ActiveAdmin.register Subscription do
  permit_params :user_id, :course_id

  show do |t|
    attributes_table do
      row :user_id, label: "Пользователь"
      row :course_id, label: "Курс"
      end
    end

  form :html => {:enctype => "multipart/form-data"} do |f|
    f.inputs do
      f.input :user_id, label: "Пользователь"
      f.input :course_id, label: "Курс"
    end
    f.actions
  end

end
