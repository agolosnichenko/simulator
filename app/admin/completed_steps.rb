ActiveAdmin.register CompletedStep do
  permit_params :user_id, :step_id

  show do |t|
    attributes_table do
      row :user_id, label: "Пользователь"
      row :step_id, label: "Шаг"
      row :useranswer, label: "Ответ пользователя"
      row :correct, label: "Верный?"
      end
    end

  form :html => {:enctype => "multipart/form-data"} do |f|
    f.inputs do
      f.input :user_id, label: "Пользователь"
      f.input :step_id, label: "Шаг"
    end
    f.actions
  end

end
