ActiveAdmin.register Step do

# See permitted parameters documentation:
# https://github.com/activeadmin/activeadmin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
#
# permit_params :list, :of, :attributes, :on, :model
#
# or
#
# permit_params do
#   permitted = [:permitted, :attributes]
#   permitted << :other if params[:action] == 'create' && current_user.admin?
#   permitted
# end
  permit_params :lesson_id, :step_type, :bot_id, :content, :option1, :option2,
                :option3, :option4, :option5, :option6, :option7, :option8,
                :correctanswer, :tag, :message_if_correct, :message_if_incorrect
  sortable tree: false, sorting_attribute: :tag
  index :as => :sortable do
    label :content
    actions
  end
  index do
    selectable_column
    column :lesson, label: "Урок"
    column :step_type, label: "Тип шага"
    column :bot, label: "От кого"
    column :content, label: "Содержание"
    column :option1, label: "Вариант ответа 1"
    column :option2, label: "Вариант ответа 2"
    column :option3, label: "Вариант ответа 3"
    column :option4, label: "Вариант ответа 4"
    column :option5, label: "Вариант ответа 5"
    column :option6, label: "Вариант ответа 6"
    column :option7, label: "Вариант ответа 7"
    column :option8, label: "Вариант ответа 8"
    column :correctanswer, label: "Правильный ответ"
    column :message_if_correct, label: "Сообщение при правильном ответе"
    column :message_if_incorrect, label: "Сообщение при неправильном ответе"
    column :tag, label: "Тег"
    actions
  end

  form do |f|
    f.inputs do
      input :lesson, label: "Урок"
      input :step_type, label: "Тип шага", :as => :select, :collection =>
                                      ['message', 'test_input', 'test_checkbox',
                                      'test_radio']
      input :bot, label: "От кого"
      input :content, label: "Содержание", :input_html => { :class => "tinymce" }, :as => :text
      input :option1, label: "Вариант ответа 1"
      input :option2, label: "Вариант ответа 2"
      input :option3, label: "Вариант ответа 3"
      input :option4, label: "Вариант ответа 4"
      input :option5, label: "Вариант ответа 5"
      input :option6, label: "Вариант ответа 6"
      input :option7, label: "Вариант ответа 7"
      input :option8, label: "Вариант ответа 8"
      input :correctanswer, label: "Правильный ответ"
      input :message_if_correct, label: "Сообщение при правильном ответе"
      input :message_if_incorrect, label: "Сообщение при неправильном ответе"

    end
    actions
  end

end
