ActiveAdmin.register Course do
  permit_params :name, :image, :description

  show do |t|
    attributes_table do
      row :name, label: "Название"
      row :description, label: "Описание"
      row :image, label: "Изображение"
      end
    end

  form :html => {:enctype => "multipart/form-data"} do |f|
    f.inputs do
      f.input :name, label: "Название"
      f.input :description, label: "Описание"
      f.file_field :image, label: "Изображение"
    end
    f.actions
  end

end
